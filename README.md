# Support Engineer Code Challenge

This is a repository for solving the Code Challenge from Trufla Technologies for Support Engineer position.

_____________________________________________________________________________________________________________________

## **JS Challenge**

Code implemented the stack data structure methods 'push', 'pop', 'peek' and 'swap', and exported a 'stack' function which switched on 'stackOperation' passed on parameter to select a method to apply, after investigation and testing, bugs were found:

1. push: didn't increase 'count' index before pushing the new element.
2. peek: returned array of non expected value.
3. swap: returned the same original stack without applying any swapping functionality.
4. On 'switch(stackOperation)' methods calls needed to be returned.
5. Export syntax wasn't correct.

Attached [stack.js](https://gitlab.com/nada-teleb/support-engineer-code-challenge/-/blob/master/stack.js) code file is fixed and tested.

_____________________________________________________________________________________________________________________

## **Python Challenge**

Code implemented 'buying_candy' function which took an integer parameter 'amount_of_money' and returned an integer representing number of unique ways to buy candy, after investigation and testing, bugs were found:

1. Check for 0 or negative amount_of_money was missing.
2. Implemented computation logic was unaccurate, including:
- 2.1. Incorrect initial counter value.
- 2.2. Incorrect 'while' condition.
- 2.3. Counter increment missing.

Attached [candy-store.py](https://gitlab.com/nada-teleb/support-engineer-code-challenge/-/blob/master/candy-store.py) code file is fixed and tested.

_____________________________________________________________________________________________________________________

## **MySQL Challenge**

Provided code didn't consider required conditions to select the culture carriers, following restrictions needed to be added:

1. Check to represent zero-employees countries.
2. Calculation for longest working period for employees working in multiple offices.
3. Check for culture carriers to have at least 1 year of working experience in at least one location.
4. Exclusion for employees no longer working for the company.
5. Ordering the list by number of employees, descending, and by country name when countries have same number of culture carriers employees.

CODE:

```sql
SELECT c.name country, ifnull(COUNT(ewc.employee_id),0) as culture_carriers FROM sys.country c

LEFT OUTER JOIN sys.working_center wc on  wc.country_id = c.id
LEFT OUTER JOIN sys.employee_working_center ewc on wc.id = ewc.working_center_id
LEFT OUTER JOIN (SELECT ewc.employee_id as employee_id, MIN(ewc.start_date) as start_date FROM sys.employee_working_center ewc
GROUP BY EWC.employee_id) as mwsd on ewc.employee_id = mwsd.employee_id

where ewc.start_date = mwsd.start_date AND ewc.start_date < adddate(now(), -365) OR ewc.employee_id IS NULL
group by c.name order by culture_carriers desc, country asc;
```

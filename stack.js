function stack(stackOperation, stackValue) {
	var stackHolder = {
		count: 3,
		storage: [
			1,
			'{id: 1,value: "obj"}',
			"stringHolder",
			46
		]
	};

	var push = function (value) {
		stackHolder.count++;
		stackHolder.storage[stackHolder.count] = value;

		return stackHolder.storage;
	}

	var pop = function () {
		if (stackHolder.count === 0) {
			return [];
		}

		var poppedItem = stackHolder.storage[stackHolder.count];
		delete stackHolder.storage[stackHolder.count];
		stackHolder.count--;

		return poppedItem;
	}

	var peek = function () {
		var peekedItem = stackHolder.storage[stackHolder.count];

		return peekedItem;
	}

	var swap = function () {
		var topItem = stackHolder.storage[stackHolder.count];
		var secondToTopItem = stackHolder.storage[stackHolder.count-1];

		stackHolder.storage[stackHolder.count] = secondToTopItem;
		stackHolder.storage[stackHolder.count-1] = topItem;

		return stackHolder.storage;
	}

	switch (stackOperation) {
		case 'push':
			return push(stackValue);
		case 'pop':
			return pop();
		case 'swap':
			return swap();
		case 'peek':
			return peek();
		default:
			return stackHolder.storage;
	}
};

module.exports = stack;